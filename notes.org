| Number        | Name          | Comments                                     |
* registers
| number        | name          | comments                                     |
|---------------+---------------+----------------------------------------------+
| $0            | $zero         | Always zero                                  |
| $1            | $at           | Reserved for assembler                       |
| $2, $3        | $v0, $v1      | First and second return values, respectively |
| $4, ..., $7   | $a0, ..., $a3 | First four arguments to functions            |
| $8, ..., $15  | $t0, ..., $t7 | Temporary registers                          |
| $16, ..., $23 | $s0, ..., $s7 | Saved registers                              |
| $24, $25      | $t8, $t9      | More temporary registers                     |
| $26, $27      | $k0, $k1      | Reserved for kernel (operating system)       |
| $28           | $gp           | Global pointer                               |
| $29           | $sp           | Stack pointer                                |
| $30           | $fp           | Frame pointer                                |
| $31           | $ra           | Return address                               |

* opcodes
| name | hex number | binary |
|------+------------+--------|
| addi | 8          | 001000 |
| sw   | 2b         | 101011 |
|      |            |        |

* test instructions

| R-instruction     | opcode rs    rt    rd    shamt funct  |                           concat |
| add $s0, $s1, $s2 | 000000 10000 10001 10010 00000 010100 | 00000010000100011001000000010100 |
| I-instruction     | opcode rs    rt    immediate          |                           concat |
| sw $s0, $s0, 4    | 101011 10000 10000 0000000000000100   | 10101110000100000000000000000100 |
| sw $s1, $s1, 3    | 101011 10001 10001 0000000000000011   | 10101110001100010000000000000011 |

 00000010000100011001000000010100
 10101110000100000000000000000100
 10101110001100010000000000000011

* run vivado in VM
sudo /home/edsp_lab/Xilinx/Vivado/2016.4/bin/vivado
