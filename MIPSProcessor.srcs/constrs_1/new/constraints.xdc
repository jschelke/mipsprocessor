#create_clock -name gclk -period 10 [get_ports "clk"]
#set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets "clk"]

#set_property -dict "PACKAGE_PIN F22 IOSTANDARD LVCMOS33" [get_ports "ProcData_OUT[0]"]
#set_property -dict "PACKAGE_PIN G22 IOSTANDARD LVCMOS33" [get_ports "ProcData_OUT[1]"]
#set_property -dict "PACKAGE_PIN H22 IOSTANDARD LVCMOS33" [get_ports "ProcData_OUT[2]"]
#set_property -dict "PACKAGE_PIN F21 IOSTANDARD LVCMOS33" [get_ports "ProcData_OUT[3]"]
#set_property -dict "PACKAGE_PIN H19 IOSTANDARD LVCMOS33" [get_ports "ProcData_OUT[4]"]
#set_property -dict "PACKAGE_PIN H18 IOSTANDARD LVCMOS33" [get_ports "ProcData_OUT[5]"]
#set_property -dict "PACKAGE_PIN H17 IOSTANDARD LVCMOS33" [get_ports "ProcData_OUT[6]"]
#set_property -dict "PACKAGE_PIN M15 IOSTANDARD LVCMOS33" [get_ports "ProcData_OUT[7]"]

## REPLACING GPIOs
#set_property -dict "PACKAGE_PIN P22 IOSTANDARD LVCMOS33" [get_ports "ProcData_OUT[11]"]
#set_property -dict "PACKAGE_PIN M17 IOSTANDARD LVCMOS33" [get_ports "ProcData_OUT[12]"]
#set_property -dict "PACKAGE_PIN M21 IOSTANDARD LVCMOS33" [get_ports "ProcData_OUT[13]"]
#set_property -dict "PACKAGE_PIN K19 IOSTANDARD LVCMOS33" [get_ports "ProcData_OUT[14]"]
#set_property -dict "PACKAGE_PIN P20 IOSTANDARD LVCMOS33" [get_ports "ProcData_OUT[15]"]
#set_property -dict "PACKAGE_PIN P17 IOSTANDARD LVCMOS33" [get_ports "ProcData_OUT[16]"]
#set_property -dict "PACKAGE_PIN P21 IOSTANDARD LVCMOS33" [get_ports "ProcData_OUT[17]"]
#set_property -dict "PACKAGE_PIN P18 IOSTANDARD LVCMOS33" [get_ports "ProcData_OUT[18]"]



## On-board Left, Right, Up, Down, and Select Pushbuttons

#set_property -dict "PACKAGE_PIN N15 IOSTANDARD LVCMOS33" [get_ports "ProcData_OUT[19]"]
#set_property -dict "PACKAGE_PIN R18 IOSTANDARD LVCMOS33" [get_ports "ProcData_OUT[20]"]
#set_property -dict "PACKAGE_PIN T18 IOSTANDARD LVCMOS33" [get_ports "ProcData_OUT[21]"]
#set_property -dict "PACKAGE_PIN R16 IOSTANDARD LVCMOS33" [get_ports "ProcData_OUT[22]"]
#set_property -dict "PACKAGE_PIN P16 IOSTANDARD LVCMOS33" [get_ports "ProcData_OUT[23]"]

## Pmod JA

##set_property -dict "PACKAGE_PIN Y11 IOSTANDARD LVCMOS33" [get_ports "PS_GPIO[24]"]
#set_property -dict "PACKAGE_PIN Y11 IOSTANDARD LVCMOS33" [get_ports "ProcData_OUT[24]"]
#set_property -dict "PACKAGE_PIN AA11 IOSTANDARD LVCMOS33" [get_ports "ProcData_OUT[8]"]
#set_property -dict "PACKAGE_PIN Y10 IOSTANDARD LVCMOS33" [get_ports "ProcData_OUT[9]"]
#set_property -dict "PACKAGE_PIN AA9 IOSTANDARD LVCMOS33" [get_ports "ProcData_OUT[10]"]
#set_property -dict "PACKAGE_PIN AB11 IOSTANDARD LVCMOS33" [get_ports "ProcData_OUT[28]"]
#set_property -dict "PACKAGE_PIN AB10 IOSTANDARD LVCMOS33" [get_ports "ProcData_OUT[29]"]
#set_property -dict "PACKAGE_PIN AB9 IOSTANDARD LVCMOS33" [get_ports "ProcData_OUT[30]"]
##set_property -dict "PACKAGE_PIN AA8 IOSTANDARD LVCMOS33" [get_ports "PS_GPIO[31]"]

### Pmod JB

##set_property -dict "PACKAGE_PIN W12 IOSTANDARD LVCMOS33" [get_ports "PS_GPIO[32]"]
##set_property -dict "PACKAGE_PIN W11 IOSTANDARD LVCMOS33" [get_ports "PS_GPIO[33]"]
##set_property -dict "PACKAGE_PIN V10 IOSTANDARD LVCMOS33" [get_ports "PS_GPIO[34]"]
##set_property -dict "PACKAGE_PIN W8 IOSTANDARD LVCMOS33" [get_ports "PS_GPIO[35]"]
##set_property -dict "PACKAGE_PIN V12 IOSTANDARD LVCMOS33" [get_ports "PS_GPIO[36]"]
##set_property -dict "PACKAGE_PIN W10 IOSTANDARD LVCMOS33" [get_ports "PS_GPIO[37]"]
##set_property -dict "PACKAGE_PIN V9 IOSTANDARD LVCMOS33" [get_ports "PS_GPIO[38]"]
##set_property -dict "PACKAGE_PIN V8 IOSTANDARD LVCMOS33" [get_ports "PS_GPIO[39]"]

### Pmod JC

##set_property -dict "PACKAGE_PIN AB7 IOSTANDARD LVCMOS33" [get_ports "PS_GPIO[40]"]
##set_property -dict "PACKAGE_PIN AB6 IOSTANDARD LVCMOS33" [get_ports "PS_GPIO[41]"]
##set_property -dict "PACKAGE_PIN Y4 IOSTANDARD LVCMOS33" [get_ports "PS_GPIO[42]"]
##set_property -dict "PACKAGE_PIN AA4 IOSTANDARD LVCMOS33" [get_ports "PS_GPIO[43]"]
##set_property -dict "PACKAGE_PIN R6 IOSTANDARD LVCMOS33" [get_ports "PS_GPIO[44]"]
##set_property -dict "PACKAGE_PIN T6 IOSTANDARD LVCMOS33" [get_ports "PS_GPIO[45]"]
##set_property -dict "PACKAGE_PIN T4 IOSTANDARD LVCMOS33" [get_ports "PS_GPIO[46]"]
##set_property -dict "PACKAGE_PIN U4 IOSTANDARD LVCMOS33" [get_ports "PS_GPIO[47]"]

### Pmod JD

##set_property -dict "PACKAGE_PIN V7 IOSTANDARD LVCMOS33" [get_ports "PS_GPIO[48]"]
##set_property -dict "PACKAGE_PIN W7 IOSTANDARD LVCMOS33" [get_ports "PS_GPIO[49]"]
##set_property -dict "PACKAGE_PIN V5 IOSTANDARD LVCMOS33" [get_ports "PS_GPIO[50]"]
##set_property -dict "PACKAGE_PIN V4 IOSTANDARD LVCMOS33" [get_ports "PS_GPIO[51]"]
##set_property -dict "PACKAGE_PIN W6 IOSTANDARD LVCMOS33" [get_ports "PS_GPIO[52]"]
##set_property -dict "PACKAGE_PIN W5 IOSTANDARD LVCMOS33" [get_ports "PS_GPIO[53]"]
##set_property -dict "PACKAGE_PIN U6 IOSTANDARD LVCMOS33" [get_ports "PS_GPIO[54]"]
##set_property -dict "PACKAGE_PIN U5 IOSTANDARD LVCMOS33" [get_ports "PS_GPIO[55]"]