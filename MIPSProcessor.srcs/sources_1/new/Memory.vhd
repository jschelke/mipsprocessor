library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.all;

entity Memory is
  port (clk          : in    STD_LOGIC;
        write_enable : in    STD_LOGIC;
        read_enable  : in    STD_LOGIC;
        address      : in    STD_LOGIC_VECTOR(31 downto 0);
        data_in      : in    STD_LOGIC_VECTOR(31 downto 0);
        data_out     : out   STD_LOGIC_VECTOR(31 downto 0) := (others => '0')
        );
end Memory;

architecture Behavioral of Memory is

type memory is array (0 to 255) of STD_LOGIC_VECTOR(31 downto 0);
signal ram : memory := (others => "00000000000000000000000000000000");

begin

ReadWrite:process(clk)
begin
    if(rising_edge(clk)) then
        if (write_enable = '1') then
            ram(to_integer(unsigned(address))) <= data_in;
        end if;
        if (read_enable = '1') then
             data_out <= ram(to_integer(unsigned(address)));
        end if;
    end if;
end process ReadWrite;

end Behavioral;
