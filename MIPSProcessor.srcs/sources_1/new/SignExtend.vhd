library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity SignExtend is
  port (immediate    : in    STD_LOGIC_VECTOR(15 downto 0);
        extended     : out   STD_LOGIC_VECTOR(31 downto 0));
end SignExtend ;

architecture Behavioral of SignExtend is

begin

extension:process(immediate)
begin
  if (immediate(15) = '1') then
    extended <= "1111111111111111" & immediate;
  else
    extended <= "0000000000000000" & immediate;
  end if;

end process extension;



end Behavioral;
