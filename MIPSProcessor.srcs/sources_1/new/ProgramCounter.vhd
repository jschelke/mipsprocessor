----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 10/22/2019 11:08:15 AM
-- Design Name: 
-- Module Name: ProgramCounter - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;



entity ProgramCounter is
    Port (  reset       : in    STD_LOGIC;
            clk         : in    STD_LOGIC;
    		PC_IN       : in    STD_LOGIC_VECTOR(31 downto 0);
    		PC_OUT      : out   STD_LOGIC_VECTOR(31 downto 0):= (others => '0'));
end ProgramCounter;

architecture Behavioral of ProgramCounter is

signal PC_RESET     :   STD_LOGIC := '0';

begin

resetPC:process(reset, clk)
begin
    if(reset = '1') then
        PC_RESET <= '1';
    elsif ((rising_edge(clk)) and (reset = '0')) then
        PC_RESET <= '0';
    end if;
end process resetPC;

setPC:process(clk)
begin
    if(rising_edge(clk)) then
        if(PC_RESET = '0') then
            PC_OUT <= PC_IN;
        else
            PC_OUT <= (others => '0');
        end if;
    end if;
end process setPC;

end Behavioral;
