----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 11/18/2019 01:50:52 PM
-- Design Name: 
-- Module Name: Registers - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Registers is
  port (  clk          : in    STD_LOGIC;
          write_enable : in    STD_LOGIC;
          Rs           : in    STD_LOGIC_VECTOR(4 downto 0);
          Rt           : in    STD_LOGIC_VECTOR(4 downto 0);
          Rd           : in    STD_LOGIC_VECTOR(4 downto 0);
          data_in      : in    STD_LOGIC_VECTOR(31 downto 0);
          out1         : out   STD_LOGIC_VECTOR(31 downto 0):= (others => '0');
          out2         : out   STD_LOGIC_VECTOR(31 downto 0):= (others => '0')
          );
end Registers;

architecture Behavioral of Registers is

type memory is array (0 to 31) of STD_LOGIC_VECTOR(31 downto 0);
signal registrar : memory := (others => "00000000000000000000000000000000");

begin

setRegisters:process(clk)
begin
    if(rising_edge(clk)) then
        
        if ((write_enable = '1')) then
            registrar(to_integer(unsigned(Rd))) <= data_in;
        else
        	out1 <= registrar(to_integer(unsigned(Rs)));
            out2 <= registrar(to_integer(unsigned(Rt)));
        end if;
        
        -- make sure register 0 is 0
        registrar(0) <= "00000000000000000000000000000000";
    end if;
    
end process setRegisters;


end Behavioral;
