----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 10/29/2019 10:25:38 AM
-- Design Name: 
-- Module Name: ProgramCounterAdder - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity ProgramCounterINC4 is
    Port (		clk         : in    STD_LOGIC;
    			PC_IN       : in    STD_LOGIC_VECTOR(31 downto 0);
				PC_OUT      : out   STD_LOGIC_VECTOR(31 downto 0):= (others => '0'));
end ProgramCounterINC4;

architecture Behavioral of ProgramCounterINC4 is
begin
	ADD4: process(clk)
	begin
		if(rising_edge(clk)) then
	       	PC_OUT <= std_logic_vector(unsigned(PC_IN) + 4);
	   	end if;
	end process ADD4;
end Behavioral;
