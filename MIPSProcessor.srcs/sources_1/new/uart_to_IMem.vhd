library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity uart_to_Imem is
  port(
    clk: 		IN 		STD_LOGIC;
    RX:			IN		STD_LOGIC;
    instr_ready:        OUT             STD_LOGIC;
    instr_index:    OUT             STD_LOGIC_VECTOR(6 downto 0);
    instr:	        OUT             STD_LOGIC_VECTOR(31 downto 0)
    );
end uart_to_Imem ;

architecture Behavioral of uart_to_Imem  is
  component UART
    port(
      clk: 		IN 		STD_LOGIC;
      RX:		IN		STD_LOGIC;
      RX_ready:	        OUT             STD_LOGIC;
      RX_data:	        OUT             STD_LOGIC_VECTOR(7 downto 0)
      );
  end component;

  signal RX_ready : STD_LOGIC;
  signal RX_data : STD_LOGIC_VECTOR(7 downto 0);

  signal instruction: STD_LOGIC_VECTOR(31 downto 0);


  subtype word is STD_LOGIC_VECTOR(31 downto 0);
  type memory is array(0 to 127) of word;
  signal myMemory2:memory := (others => (others => '0'));
  signal index:     unsigned(6 downto 0):=(others=>'1');

  signal counter:   unsigned(3 downto 0):=(others=>'0');
      
  type RX_STATE is (RCVDATA,STARTSEND,SENDING);
  signal state:RX_STATE:=RCVDATA;

begin
  UART_in : UART
  port map (
    clk => clk,
    RX => RX,
    RX_ready => RX_ready,
    RX_data => RX_data
    );

  bytes_to_32:process(clk)
    --variable counter:   unsigned(3 downto 0):=(others=>'0');
  begin
    if rising_edge(clk) then
    case state is
    when RCVDATA =>
      if RX_ready = '1' then
        if RX_data = x"AA" then
          counter<=(others=>'0');
          index<=index+1;
          else
          counter<=counter+1;
        end if;
      else
        case counter is
        --when x"0" =>
        when x"1" =>
            instruction(31 downto 24) <= RX_data;
        when x"2" =>
            instruction(23 downto 16) <= RX_data;
        when x"3" =>
            instruction(15 downto 8) <= RX_data;
        when x"4" =>
            instruction(7 downto 0) <= RX_data;
            counter<=counter+1;
        when x"5" =>
            mymemory2(to_integer(index))<=instruction;
            counter<=counter+1;
        when others =>
        end case;  
      end if;
      
      if mymemory2(to_integer(index)) = x"EE00EE00" then
        state<=STARTSEND;
      end if;
    when STARTSEND =>
        index<=(others=>'0');
        state<=SENDING;
    when SENDING =>
        if mymemory2(to_integer(index)) = x"EE00EE00" then
            instr_ready<='0';
            index<=(others=>'0');
            state<=RCVDATA;
        else
            instr_ready<='1';
            index<=index+1;
            instr<=mymemory2(to_integer(index));
            instr_index<=(std_logic_vector(index));
        end if;
    when others =>
    end case;
    end if;
  end process bytes_to_32;

--  send_to_Imem:process(clk)
--  begin
--    if rising_edge(clk) then
--        if state = SENDING then
         
--        end if;
--      end if;
--  end process send_to_Imem;

end Behavioral;
