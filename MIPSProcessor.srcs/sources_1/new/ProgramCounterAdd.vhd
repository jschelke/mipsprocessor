----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 11/18/2019 05:26:33 PM
-- Design Name: 
-- Module Name: ProgramCounterAdd - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity ProgramCounterAdd is
    Port ( clk 		: in 	STD_LOGIC;
           PC_IN 	: in 	STD_LOGIC_VECTOR (31 downto 0);
           PC_OUT 	: out 	STD_LOGIC_VECTOR (31 downto 0):= (others => '0');
           IR_IN 	: in 	STD_LOGIC_VECTOR (31 downto 0));
end ProgramCounterAdd;

architecture Behavioral of ProgramCounterAdd is
	

begin
	ADD: process(clk)
	begin
		if(rising_edge(clk)) then
       		PC_OUT <= std_logic_vector(unsigned(PC_IN) + (unsigned(IR_IN) sll 2));
   		end if;
	end process ADD;

end Behavioral;
