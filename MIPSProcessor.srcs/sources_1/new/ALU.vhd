library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use ieee.STD_LOGIC_SIGNED.ALL;

entity ALU is
  port (clk          : in    STD_LOGIC;
        alu_crtl     : in    STD_LOGIC_VECTOR(4 downto 0);
        in1          : in    STD_LOGIC_VECTOR(31 downto 0);
        in2          : in    STD_LOGIC_VECTOR(31 downto 0);
        result       : out   STD_LOGIC_VECTOR(31 downto 0):= (others => '0');
        zero         : out   STD_LOGIC);
end ALU;

architecture Behavioral of ALU is

	constant  and_code 	   : STD_LOGIC_VECTOR(4 downto 0):="00000";
	constant  or_code  	   : STD_LOGIC_VECTOR(4 downto 0):="00001";
	constant  add_code 	   : STD_LOGIC_VECTOR(4 downto 0):="00010";
	constant  sub_code 	   : STD_LOGIC_VECTOR(4 downto 0):="00011";
	constant  addu_code    : STD_LOGIC_VECTOR(4 downto 0):="00100";
	constant  nor_code     : STD_LOGIC_VECTOR(4 downto 0):="00101";
	constant  subu_code    : STD_LOGIC_VECTOR(4 downto 0):="00110";
	constant  xor_code 	   : STD_LOGIC_VECTOR(4 downto 0):="00111";
	constant  sllv_code    : STD_LOGIC_VECTOR(4 downto 0):="01000";
	constant  srav_code    : STD_LOGIC_VECTOR(4 downto 0):="01001";
	constant  srlv_code    : STD_LOGIC_VECTOR(4 downto 0):="01010";
	constant  beq_code 	   : STD_LOGIC_VECTOR(4 downto 0):="01011";
	constant  bgez_code    : STD_LOGIC_VECTOR(4 downto 0):="01100";
	constant  bgtz_code    : STD_LOGIC_VECTOR(4 downto 0):="01101";
	constant  blez_code    : STD_LOGIC_VECTOR(4 downto 0):="01110";
	constant  bltz_code    : STD_LOGIC_VECTOR(4 downto 0):="01111";
	constant  bne_code 	   : STD_LOGIC_VECTOR(4 downto 0):="10000";
	constant  lui_code     : STD_LOGIC_VECTOR(4 downto 0):="10001";
	constant  multu_code   : STD_LOGIC_VECTOR(4 downto 0):="10010";
	constant  divu_code    : STD_LOGIC_VECTOR(4 downto 0):="10100";
	constant  mfhi_code    : STD_LOGIC_VECTOR(4 downto 0):="10101";
	constant  mflo_code    : STD_LOGIC_VECTOR(4 downto 0):="10110";
	constant  mthi_code    : STD_LOGIC_VECTOR(4 downto 0):="10111";
	constant  mtlo_code    : STD_LOGIC_VECTOR(4 downto 0):="11000";                                                      
	constant  div_code     : STD_LOGIC_VECTOR(4 downto 0):="11001";
	constant  mult_code    : STD_LOGIC_VECTOR(4 downto 0):="11010";

	signal    mult_res     : STD_LOGIC_VECTOR(63 downto 0):= (others => '0');
	
begin

alu_operation:process(clk)
begin
    if(rising_edge(clk)) then
      case (alu_crtl) is
        when and_code => -- AND
          result <= in1 and in2;
        when or_code => -- OR
          result <= in1 or in2;
        when add_code => -- ADD
          result <= std_logic_vector(signed(in1) + signed(in2));
        when sub_code => -- SUB
          result <= std_logic_vector(signed(in1) - signed(in2));
        when addu_code => --ADDU
          result <= std_logic_vector(unsigned(in1)+unsigned(in2));
        when nor_code => -- NOR
            result <= in1 nor in2;
        when subu_code => --SUBU
            result <= std_logic_vector(unsigned(in1) - unsigned(in2));
        when xor_code => --XOR
            result <= in1 xor in2;    
        when sllv_code => --SLLV
            result <= std_logic_vector(shift_left(signed(in2), natural(to_integer(unsigned(in1)))));   
        when srav_code => --SRAV
            result <= std_logic_vector(shift_right(signed(in2), natural(to_integer(unsigned(in1)))));  
        when srlv_code => --SRLV
            result <= std_logic_vector(shift_right(unsigned(in2), natural(to_integer(unsigned(in1)))));
             
        when beq_code  => 
        	if(in1 = in2) then
        		zero <= '1';
        	else
        		zero <= '0';
        	end if; 
        	
        when bgez_code  =>
        	if(in2 = "00001") then
        		if(signed(in1) >= 0) then
        			zero <= '1';
        		else
        			zero <= '0';
        		end if; 
        	elsif(in2 = "100000") then
        		if(signed(in1) < 0) then
        	        zero <= '1';
        		else
        	        zero <= '0';
        	    end if; 
        	end if;
        	
        when bgtz_code  => 
        	if(signed(in1) > 0) then
        		zero <= '1';
			else
        		zero <= '0';
        	end if; 
        	
        when blez_code  => 
        	if(signed(in1) <= 0) then
        		zero <= '1';
			else
        		zero <= '0';
        	end if; 
        	
        when bne_code  => 
        	if(signed(in1) /= 0) then
        		zero <= '1';
			else
        		zero <= '0';
        	end if; 
        	
        when lui_code =>
            result <= std_logic_vector(shift_left(signed(in2), natural(16)));
--            result <= "11110000000000000000000000000000";
        when mult_code =>
            mult_res <= std_logic_vector(to_signed(to_integer(signed(in1))*to_integer(signed(in2)),64));
        
        when multu_code =>
            mult_res <= std_logic_vector(to_unsigned(to_integer(unsigned(in1))*to_integer(unsigned(in2)),64));
            
        when div_code =>
            mult_res(31 downto 0)  <= std_logic_vector(to_signed(to_integer(signed(in1))/ to_integer(signed(in2)), 32));
            mult_res(63 downto 32) <= std_logic_vector(to_signed(to_integer(signed(in1)) mod to_integer(signed(in2)), 32));
            
        when divu_code =>
            mult_res(31 downto 0)  <= std_logic_vector(to_unsigned(to_integer(unsigned(in1))/ to_integer(unsigned(in2)), 32));
            mult_res(63 downto 32) <= std_logic_vector(to_unsigned(to_integer(unsigned(in1)) mod to_integer(unsigned(in2)), 32));  
            
            
        when mfhi_code =>
            result <= mult_res(63 downto 32);
        
        when mflo_code =>
            result <= mult_res(31 downto 0);
        
        when mthi_code =>
            mult_res(63 downto 32) <= in1;
        
        when mtlo_code =>
            mult_res(31 downto 0) <= in1;
                
        when others =>
          result <= in1 + in2;
      end case;
    end if;

end process alu_operation;


end Behavioral;
