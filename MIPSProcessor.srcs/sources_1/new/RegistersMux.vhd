----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 11/18/2019 02:40:59 PM
-- Design Name: 
-- Module Name: RegistersMux - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;


entity RegistersMux is
    Port (  enable      : in    STD_LOGIC;
    		Rt          : in    STD_LOGIC_VECTOR(4 downto 0);
    		Rd          : in    STD_LOGIC_VECTOR(4 downto 0);
    		MuxOut      : out   STD_LOGIC_VECTOR(4 downto 0)
    		);
end RegistersMux;

architecture Behavioral of RegistersMux is

begin

setMux:process(enable, rt, Rd)
begin
    if(enable = '1') then
        MuxOut <= Rd;
    else
        MuxOut <= Rt;
    end if;
end process setMux;


end Behavioral;
