----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 11/19/2019 10:33:40 AM
-- Design Name: 
-- Module Name: PC_Top - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Control_Top is
    Port ( 	clk 			: in 	STD_LOGIC;
    		PC_OUT2 		: out 	STD_LOGIC_VECTOR(31 downto 0):= (others => '0');
    		IR_OUT2			: out 	STD_LOGIC_VECTOR(31 downto 0):= (others => '0');
    		ALU_IN1		    : out 	STD_LOGIC_VECTOR(31 downto 0):= (others => '0');
    		ALU_IN2   		: out 	STD_LOGIC_VECTOR(31 downto 0):= (others => '0');
    		ProcData_OUT	: out 	STD_LOGIC_VECTOR(31 downto 0):= (others => '0');
    		RX              : in    STD_LOGIC
    );
end Control_Top;

architecture Behavioral of Control_Top is
	component PC_Top is
    Port ( 	clk_in 			: in 	STD_LOGIC;
			PC_JMP			: in 	STD_LOGIC_VECTOR(25 downto 0);
			PC_BR			: in 	STD_LOGIC_VECTOR(31 downto 0);
			PCSrc			: in 	STD_LOGIC_VECTOR(1 downto 0);
			OpCode			: out 	STD_LOGIC_VECTOR(31 downto 0);
			IR_Done			: in 	STD_LOGIC; --Instruction is done now send new information
			IR_NewServed 	: out	STD_LOGIC := '0';
			PC_Output		: out	STD_LOGIC_VECTOR(31 downto 0) := (others => '0');
    		RX              : in    STD_LOGIC
			);
	end component PC_Top;
	
	component registers is
	port (  clk          : in    STD_LOGIC;
	        write_enable : in    STD_LOGIC;
	        Rs           : in    STD_LOGIC_VECTOR(4 downto 0);
	        Rt           : in    STD_LOGIC_VECTOR(4 downto 0);
	        Rd           : in    STD_LOGIC_VECTOR(4 downto 0);
	        data_in      : in    STD_LOGIC_VECTOR(31 downto 0);
	        out1         : out   STD_LOGIC_VECTOR(31 downto 0);
	        out2         : out   STD_LOGIC_VECTOR(31 downto 0)
	        );
	end component registers;
	
	component RegistersMUX
    Port (  enable      : in    STD_LOGIC;
			Rt          : in    STD_LOGIC_VECTOR(4 downto 0);
			Rd          : in    STD_LOGIC_VECTOR(4 downto 0);
			MuxOut      : out   STD_LOGIC_VECTOR(4 downto 0)
			);
	end component RegistersMUX;
	
	component ALUControl
	port (	clk      	: in     STD_LOGIC;
	     	opcode   	: in     STD_LOGIC_VECTOR(5 downto 0);
	      	funct    	: in     STD_LOGIC_VECTOR(5 downto 0);
	      	alu_crtl 	: out    STD_LOGIC_VECTOR(4 downto 0)
	      	);
	end component ALUControl;
	
	component ALU
	port (	clk          : in    STD_LOGIC;
	      	alu_crtl     : in    STD_LOGIC_VECTOR(4 downto 0);
	      	in1          : in    STD_LOGIC_VECTOR(31 downto 0);
	      	in2          : in    STD_LOGIC_VECTOR(31 downto 0);
	      	result       : out   STD_LOGIC_VECTOR(31 downto 0);
	      	zero         : out   STD_LOGIC
	      	);
	end component ALU;
	
	component SignExtend
	port (	immediate    : in    STD_LOGIC_VECTOR(15 downto 0);
		    extended     : out   STD_LOGIC_VECTOR(31 downto 0)
		   	);
	end component SignExtend;
	
	component Memory
	port (	clk          : in    STD_LOGIC;
	      	write_enable : in    STD_LOGIC;
	      	read_enable  : in    STD_LOGIC;
	      	address      : in    STD_LOGIC_VECTOR(31 downto 0);
	      	data_in      : in    STD_LOGIC_VECTOR(31 downto 0);
	      	data_out     : out   STD_LOGIC_VECTOR(31 downto 0)
	      	);
	end component Memory;
	
	component MUX32
	Port (  enable      : in    STD_LOGIC;
			in0         : in    STD_LOGIC_VECTOR(31 downto 0);
			in1         : in    STD_LOGIC_VECTOR(31 downto 0);
			MuxOut      : out   STD_LOGIC_VECTOR(31 downto 0)
				);
	end component MUX32;
	
	
	-- Opcodes
	
	constant ARITHMATIC    : STD_LOGIC_VECTOR(5 downto 0) := "000000";
	constant ADDI          : STD_LOGIC_VECTOR(5 downto 0) := "001000";
	constant ADDIU         : STD_LOGIC_VECTOR(5 downto 0) := "001001";
    constant ANDI          : STD_LOGIC_VECTOR(5 downto 0) := "001100";
    constant LUI           : STD_LOGIC_VECTOR(5 downto 0) := "001111";
    constant ORI           : STD_LOGIC_VECTOR(5 downto 0) := "001101";
    constant XORI          : STD_LOGIC_VECTOR(5 downto 0) := "001110";
    constant BEQ           : STD_LOGIC_VECTOR(5 downto 0) := "000100";
    constant BGEZ          : STD_LOGIC_VECTOR(5 downto 0) := "000001";
    constant BGTZ          : STD_LOGIC_VECTOR(5 downto 0) := "000111";
    constant BLEZ          : STD_LOGIC_VECTOR(5 downto 0) := "000110";
    constant BNE           : STD_LOGIC_VECTOR(5 downto 0) := "000101";
    constant LW            : STD_LOGIC_VECTOR(5 downto 0) := "100011";
    constant SW            : STD_LOGIC_VECTOR(5 downto 0) := "101011";
--    constant JR            : STD_LOGIC_VECTOR(5 downto 0) := "001000";
    constant J             : STD_LOGIC_VECTOR(5 downto 0) := "000010";
    
	--Clock Signals
    signal clk_PC 			: STD_LOGIC := '0';
	signal clk_reg 			: STD_LOGIC := '0';
	signal clk_ALUCONTROL 	: STD_LOGIC := '0';
	signal clk_ALU			: STD_LOGIC := '0';
	signal clk_Mem 			: STD_LOGIC := '0';
    
    -- Data Signals
    
    signal PC_INC4		: STD_LOGIC_VECTOR(31 downto 0) := (others => '0');
    signal PC_MUX		: STD_LOGIC_VECTOR(31 downto 0) := (others => '0');
    signal PC_ADD		: STD_LOGIC_VECTOR(31 downto 0) := (others => '0');
    signal REG_OUT1		: STD_LOGIC_VECTOR(31 downto 0) := (others => '0');																	
    signal REG_OUT2		: STD_LOGIC_VECTOR(31 downto 0) := (others => '0');
    signal ALU_RES		: STD_LOGIC_VECTOR(31 downto 0) := (others => '0');
    
    
    --signal Instruction
    signal reset		: STD_LOGIC := '0';
    signal OPCode		: STD_LOGIC_VECTOR(31 downto 0) := (others => '0');
    signal RegisterWr	: STD_LOGIC_VECTOR(4 downto 0);
    signal ProcData		: STD_LOGIC_VECTOR(31 downto 0) := (others => '0');
    signal alu_crtl		: STD_LOGIC_VECTOR(4 downto 0);
    signal ALU_IN_2		: STD_LOGIC_VECTOR(31 downto 0) := (others => '0');
    signal IR_EXTENDED	: STD_LOGIC_VECTOR(31 downto 0) := (others => '0');
    signal MemData		: STD_LOGIC_VECTOR(31 downto 0) := (others => '0');
    signal PCSrc		: STD_LOGIC_VECTOR(1 downto 0)  := (others => '0');
    
    --State Signals
    signal IR_NewServed	: STD_LOGIC := '0';
    signal ALUZERO		: STD_LOGIC := '0';
    signal PC_OUT		: STD_LOGIC_VECTOR(31 downto 0) := (others => '0');
    
    --Control Signals
    signal IR_Done		: STD_LOGIC := '0';
    
    signal reg_wr_en	: STD_LOGIC := '0';
    signal reg_MUXCtrl	: STD_LOGIC := '0';
    
    signal alu_op		: STD_LOGIC_VECTOR(1 downto 0) := (others => '0');
    signal ALUSrc		: STD_LOGIC := '0';
    signal MemtoReg		: STD_LOGIC := '0';
    signal Branch		: STD_LOGIC := '0';
    signal Jmp		    : STD_LOGIC := '0';
    
    signal MemWrite		: STD_LOGIC := '0';
    signal MemRead		: STD_LOGIC := '0';
    
    signal PCReady		: STD_LOGIC := '0'; -- when 1 new IR Done command can be sent
    
    --UART Signals
    --signal RX           : STD_LOGIC := '0';
    
begin
	-- Main Program Counter Component
	Comp_PC: PC_Top
	port map (
		clk_in			=> clk,
		PC_JMP			=> Opcode(25 downto 0),
		PC_BR           => IR_EXTENDED,
		PCSrc			=> PCSrc,
		OpCode			=> Opcode,
		IR_Done			=> IR_Done,
		IR_NewServed	=> IR_NewServed,
		PC_Output		=> PC_OUT,
		RX              => RX
	);
	
	-- Registers component
	Comp_Registers: registers
	port map (
		clk     		=> clk_reg,
		write_enable	=> reg_wr_en,
		Rs				=> Opcode(25 downto 21),
		Rt				=> Opcode(20 downto 16),
		Rd				=> RegisterWr,
		data_in			=> ProcData,
		out1			=> REG_OUT1,
		out2			=> REG_OUT2
	);
	-- Register Multiplexer Component
	Comp_RegistersMUX: RegistersMUX
	port map (
	   	enable			=> reg_MUXCtrl,
		Rt				=> Opcode(20 downto 16),
		Rd				=> Opcode(15 downto 11),
		MuxOut			=> RegisterWr
	);
	
	--ALU Control component
	Comp_ALUControl: ALUControl
	port map (
		clk      		=> clk_ALUCONTROL,
		opcode   		=> Opcode(31 downto 26),
		funct    		=> Opcode(5 downto 0),
		alu_crtl 		=> alu_crtl
	);
	
	--ALU component
	Comp_ALU: ALU
	port map (
		clk				=> clk_ALU,
		alu_crtl		=> alu_crtl,
		in1   			=> REG_OUT1,
		in2				=> ALU_IN_2,
		result			=> ALU_RES,
		zero			=> ALUZERO
	);
	
	-- Register Multiplexer Component
	Comp_ALUMUX: MUX32
	port map (
	   	enable			=> ALUSrc,
		in0				=> REG_OUT2,
		in1				=> IR_EXTENDED,
		MuxOut			=> ALU_IN_2
	);
	
	-- Sign Extend Component
	Comp_SignExt: SignExtend
	port map (
		immediate		=> Opcode(15 downto 0),
		extended		=> IR_EXTENDED
	);
	
	-- Memory component
	Comp_MEM: Memory
	port map (
		clk				=> clk_Mem,
		write_enable	=> MemWrite,
		read_enable		=> MemRead,
		address			=> ALU_RES,
		data_in			=> REG_OUT2,
		data_out		=> MemData
	);
	
	--Memory multiplexer Component
	Comp_MEMMUX: MUX32
	port map (
		enable			=> MemtoReg,
		in0				=> ALU_RES,
		in1				=> MemData,
		MuxOut			=> ProcData
	);
	
	ALU_IN1 <= REG_OUT1;
	ALU_IN2 <= ALU_IN_2;
    ProcData_OUT <= ProcData;
    IR_OUT2 <= Opcode;
	
	PCSrc <= Jmp & (Branch and ALUZERO);
	
	 -- Stimulus process
	 Control_Top: process(clk)
		variable CurrentState : integer := 0;
	 begin
	 	if(rising_edge(clk)) then --When clock is input check state of PC system
			case (CurrentState) is
				when 0 => --Ready to accept new Instruction
					clk_PC <= '1';
					
					if(IR_NewServed = '1') then
						currentState := 1;
						PC_OUT2 <= PC_OUT;
						
						Branch <= '0';	
						Jmp <= '0';
						
					end if;
					reg_wr_en <= '0';
				when 1 => -- New instruction is presented now decide which control needs to happen
					-- so far for ADD, ADDI, LOAD, STORE load register values
					clk_reg <= '1';

					-- set ALU MUX
					case(Opcode(31 downto 26)) is
						--arithmatic and branch
						when ARITHMATIC|BEQ|BGEZ|BGTZ|BLEZ|BNE =>
							ALUSrc <= '0';
																					
						--load, write, ADDIU, ANDI, ORI, XORI LUI and ADDI
						when ADDI|ADDIU|ANDI|LUI|ORI|XORI|LW|SW =>
							ALUSrc <= '1';
																					
						when others =>
					end case;
					
					--set MemMUX
					if(Opcode(31 downto 26) = LW) then
						MemtoReg <= '1';
					else
						MemtoReg <= '0';
					end if;
					clk_ALUCONTROL <= '1';
					currentState := 2;
					
				when 2 => -- Step 2 of running instruction
					
					clk_ALU <= '1';
					--set register MUX
					case(Opcode(31 downto 26)) is
						when ARITHMATIC => --Arithmatic
							reg_MUXCtrl <= '1';
							reg_wr_en <= '1';
							
--							if(Opcode(5 downto 0) = J) then
--                                Jmp <= '1';
--                            end if;
						when ADDI|ADDIU|ANDI|LUI|ORI|XORI|LW => -- ADDI, LOAD, LUI
							reg_MUXCtrl <= '0';
							MemWrite <= '0';
							MemRead <= '1';
							reg_wr_en <= '1';
						when SW => -- write
							MemWrite <= '1';
							MemRead <= '0';
							
						--branching
						when BEQ|BGEZ|BGTZ|BLEZ|BNE =>
							Branch <= '1';
						-- Jumping	
						when J =>
						  Jmp <= '1';
						  reg_MUXCtrl <= '1';
                          reg_wr_en <= '1';
						when others =>
					end case;
					
					
					
					currentState := 3;
					
				when 3 => -- Step 3 of running instruction
					case(Opcode(31 downto 26)) is
						--arithmatic, ADDI, LUI
						when ARITHMATIC|ADDI|ADDIU|ANDI|LUI|ORI|XORI =>
						    if(not(Opcode(5 downto 0) = J)) then
                                clk_reg <= '1';
                                currentState := 5;
                                
                            else
                                IR_Done <= '1';
                                clk_PC <= '1';
                                currentState := 0;
                            
                            end if;
									
						--load
						when LW =>
							clk_Mem <= '1';
							currentState := 4;
							
						--Write
						when SW =>
							clk_Mem <= '1';
							currentState := 5;

							
						--branching
						when BEQ|BGEZ|BGTZ|BLEZ|BNE =>
							IR_Done <= '1';
							clk_PC <= '1';
							currentState := 0;
							
						when others =>
							currentState := 5;
							
					end case;
					
				when 4 => -- Step 4 of running instruction
					case(Opcode(31 downto 26)) is											
						when LW => -- LOAD
							
							clk_reg <= '1';
						
						when others =>
							
					end case;
					
					currentState := 5;
				when 5 => -- Finish instruction by requesting new PC
					reg_wr_en <= '0';
					IR_Done <= '1';
					clk_PC <= '1';
					currentState := 0;
					
				when others =>
			end case;
		end if;
		
	 	if(falling_edge(clk)) then
	 		case currentState is
	 			--choose ALU input
	 			when 1 =>
	 				reg_wr_en <= '0';
	 				reg_MUXCtrl <= '0';
				when others =>
	 		end case;
	 		
	 		clk_PC <= '0';
	 		clk_reg <= '0';
	 		clk_ALUCONTROL <= '0';
	 		clk_ALU <= '0';
	 		clk_Mem <= '0';
	 		IR_Done <= '0';
	 	end if;
	 end process Control_Top;
	 
end Behavioral;
