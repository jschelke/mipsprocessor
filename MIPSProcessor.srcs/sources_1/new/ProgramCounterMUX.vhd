----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 11/19/2019 10:22:00 AM
-- Design Name: 
-- Module Name: ProgramCounterMUX - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use ieee.STD_LOGIC_SIGNED.ALL;


entity ProgramCounterMUX is
    Port ( PC_IN 	: in 	STD_LOGIC_VECTOR (31 downto 0);
           BR_IN 	: in 	STD_LOGIC_VECTOR (31 downto 0);
           JMP_IN 	: in 	STD_LOGIC_VECTOR (25 downto 0);
           PC_OUT 	: out 	STD_LOGIC_VECTOR (31 downto 0):= (others => '0');
           PCSrc 	: in 	STD_LOGIC_VECTOR (1 downto 0);
           clk 		: in 	STD_LOGIC);
end ProgramCounterMUX;

architecture Behavioral of ProgramCounterMUX is

begin
	
	MUX: process(clk)
	begin
		if(rising_edge(clk)) then
			if(PCSrc = "00") then
				PC_OUT <= PC_IN;
			end if;
			if(PCSrc = "01") then
				PC_OUT <= BR_IN;
			end if;
			if((PCSrc = "10") or (PCSrc = "11")) then
                PC_OUT <= std_logic_vector(shift_left("000000"&unsigned(JMP_IN),2));
            end if;
		end if;
	end process MUX;

end Behavioral;
