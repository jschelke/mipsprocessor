----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 11/18/2019 02:40:59 PM
-- Design Name: 
-- Module Name: RegistersMux - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;


entity MUX32 is
    Port (  enable      : in    STD_LOGIC;
    		in0         : in    STD_LOGIC_VECTOR(31 downto 0);
    		in1         : in    STD_LOGIC_VECTOR(31 downto 0);
    		MuxOut      : out   STD_LOGIC_VECTOR(31 downto 0) := (others => '0')
    		);
end MUX32;

architecture Behavioral of MUX32 is

begin

setMux:process(enable, in0, in1)
begin
    if(enable = '1') then
        MuxOut <= in1;
    else
        MuxOut <= in0;
    end if;
end process setMux;


end Behavioral;

