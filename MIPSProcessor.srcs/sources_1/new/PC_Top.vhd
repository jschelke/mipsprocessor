----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 11/19/2019 10:33:40 AM
-- Design Name: 
-- Module Name: PC_Top - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity PC_Top is
    Port ( 	clk_in 			: in 	STD_LOGIC;
            PC_BR			: in 	STD_LOGIC_VECTOR(31 downto 0);
    		PC_JMP			: in 	STD_LOGIC_VECTOR(25 downto 0);
    		PCSrc			: in 	STD_LOGIC_VECTOR(1 downto 0);
    		OpCode			: out 	STD_LOGIC_VECTOR(31 downto 0);
    		IR_Done			: in 	STD_LOGIC; --Instruction is done now send new information
    		IR_NewServed 	: out	STD_LOGIC := '0';
    		PC_Output		: out	STD_LOGIC_VECTOR(31 downto 0) := (others => '0');
    		RX              : in    STD_LOGIC);
end PC_Top;

architecture Behavioral of PC_Top is
	component ProgramCounter is
    Port (  reset       : in    STD_LOGIC;
            clk         : in    STD_LOGIC;
    		PC_IN       : in    STD_LOGIC_VECTOR(31 downto 0);
    		PC_OUT      : out   STD_LOGIC_VECTOR(31 downto 0));
	end component ProgramCounter;
	
	component ProgramCounterINC4 is
	    Port (		clk         : in    STD_LOGIC;
	    			PC_IN       : in    STD_LOGIC_VECTOR(31 downto 0);
					PC_OUT      : out   STD_LOGIC_VECTOR(31 downto 0));
	end component ProgramCounterINC4;
	
	component ProgramCounterAdd
    Port (  	clk 	: in 	STD_LOGIC;
               	PC_IN 	: in 	STD_LOGIC_VECTOR (31 downto 0);
               	PC_OUT 	: out 	STD_LOGIC_VECTOR (31 downto 0);
               	IR_IN 	: in 	STD_LOGIC_VECTOR (31 downto 0));
	end component ProgramCounterAdd;
	
	component ProgramCounterMUX is
	    Port ( PC_IN 	: in 	STD_LOGIC_VECTOR (31 downto 0);
	           BR_IN 	: in 	STD_LOGIC_VECTOR (31 downto 0);
               JMP_IN   : in    STD_LOGIC_VECTOR (25 downto 0);
	           PC_OUT 	: out 	STD_LOGIC_VECTOR (31 downto 0);
	           PCSrc 	: in 	STD_LOGIC_VECTOR(1 downto 0);
	           clk 		: in 	STD_LOGIC);
	end component ProgramCounterMUX;
	
	component InstructionMemory is
	    Port (  clk         : in    STD_LOGIC;
	            Adress      : in    STD_LOGIC_VECTOR(31 downto 0);
	            OPcode      : out   STD_LOGIC_VECTOR(31 downto 0);
	            flash       : in    STD_LOGIC;
                instr_index : in    STD_LOGIC_VECTOR(6 downto 0);
                instr       : in    STD_LOGIC_VECTOR(31 downto 0));
	end component InstructionMemory;
	
	component uart_to_IMem is
	    Port (  clk         : IN 		STD_LOGIC;
                RX          : IN        STD_LOGIC;
                instr_ready : OUT       STD_LOGIC;
                instr_index : OUT       STD_LOGIC_VECTOR(6 downto 0);
                instr       : OUT       STD_LOGIC_VECTOR(31 downto 0));
    end component uart_to_IMem;

	--Clock Signals
    signal clk_PC 		: STD_LOGIC := '0';
    signal clk_PC_INC4 	: STD_LOGIC := '0';
    signal clk_PC_ADD 	: STD_LOGIC := '0';
    signal clk_PC_MUX	: STD_LOGIC := '0';
    signal clk_IM		: STD_LOGIC := '0';

    -- Data Signals
    signal PC_OUT		: STD_LOGIC_VECTOR(31 downto 0) := (others => '0');
    signal PC_INC4		: STD_LOGIC_VECTOR(31 downto 0) := (others => '0');
    signal PC_MUX		: STD_LOGIC_VECTOR(31 downto 0) := (others => '0');
    signal PC_ADD		: STD_LOGIC_VECTOR(31 downto 0) := (others => '0');
    --signal Instruction
    signal reset		: STD_LOGIC := '0';
    
    signal State		: STD_LOGIC := '0'; --when 0 next PC not ready
    
    --Internal Signals
    signal CurrentState : integer := 0;

    
    -- reflash signals
	signal flash        : STD_LOGIC := '0';
    signal instr_index  : STD_LOGIC_VECTOR(6 downto 0);
    signal instr        : STD_LOGIC_VECTOR(31 downto 0);
    
begin
	-- Main Program Counter Component
	Comp_PC: ProgramCounter
	port map (
		reset 	=> 	reset,
	   	clk		=>	clk_PC,
	    PC_IN	=>	PC_MUX,
	    PC_OUT	=>	PC_OUT
	);
	-- Program Counter Increment Component
	Comp_PC_INC: ProgramCounterINC4
	port map (
	   	clk		=>	clk_PC_INC4,
	    PC_IN	=>	PC_OUT,
	    PC_OUT	=>	PC_INC4
	);
	-- Program Counter Addition Component
	Comp_PC_ADD: ProgramCounterADD
	port map (
	   	clk		=>	clk_PC_ADD,
	    PC_IN	=>	PC_INC4,
	    PC_OUT	=>	PC_ADD,
	    IR_IN	=> 	PC_BR
	);
	-- Program Counter Multiplexer Component
	Comp_PC_MUX: ProgramCounterMUX
	port map (
	   	clk		=>	clk_PC_MUX,
	    PC_IN	=>	PC_INC4,
	    PC_OUT	=>	PC_MUX,
	    BR_IN   => 	PC_ADD,
	    JMP_IN	=>  PC_JMP,
	    PCSrc	=>  PCSrc
	);
	
	-- Program Instruction Memory Component
	Comp_IM: InstructionMemory
	port map (
		clk		=>  clk_in,
		Adress	=>	PC_OUT,
		OPcode	=>  OpCode,
		flash       => flash,
		instr_index => instr_index,
		instr       => instr
	);
	-- Program Instruction Memory Component
    Comp_UART_IM: UART_to_IMEM
    port map (
        clk         => clk_in,
        RX          => RX,
        instr_ready => flash,
        instr_index => instr_index,
        instr       => instr
    );
	
	 -- Stimulus process
	 PC_Top: process(clk_in,IR_Done)
	 	--variable CurrentState : integer := 0;
	 begin
	 	if(rising_edge(clk_in)) then --When clock is input check state of PC system
	 	 		case (CurrentState) is
	 	 			when 0 => --when starting PC cycle(INC4)
	 	 				clk_IM <= '1';
	 	 				IR_NewServed <= '1';
	 	 				clk_PC_INC4 <= '1';
	 	 				CurrentState <= 1;
	 	 				PC_Output <= PC_OUT;
	 	 			when 1 => -- When just INC 4 wait for jump if needed
	 	 				IR_NewServed <= '0';
                        -- see below
	 	 			when 2 => -- When jump is needed add is performed => go to MUX
	 	 				clk_PC_MUX <='1';
	 	 				CurrentState <= 3;
	 	 				PC_Output <= PC_OUT;
	 	 			when 3 => -- Update PC
	 	 				clk_PC <= '1';
	 	 				CurrentState <= 0;
	 	 				PC_Output <= PC_OUT;
	 	 			when others =>
	 	 				clk_PC <= '0';
	 	 				clk_PC_INC4 <= '0';
	 	 				clk_PC_ADD <= '0';
	 	 				clk_PC_MUX <= '0';
	 	 				clk_IM <= '0';
	 	 				currentState <= 0;
	 	 		end case;
	 	 	end if;
	 	 	
	 	 	if(rising_edge(IR_Done) and CurrentState = 1) then
                  if((PCSrc = b"00")or(PCSrc = b"10")) then
                      clk_PC_MUX <='1';
                      CurrentState <= 3;
                  elsif(PCSrc = b"01") then
                      clk_PC_ADD <= '1';
                      CurrentState <= 2;                       
                  end if;
              end if;

	 	 	
	 	 	if(falling_edge(clk_in)) then
	 	 		clk_PC <= '0';
	 	 	    clk_PC_INC4 <= '0';
	 	 	    clk_PC_ADD <= '0';
	 	 	    clk_PC_MUX <= '0';
	 	 	    clk_IM <= '0';
	 	 	end if;
	 end process PC_Top;

	PC_reset: process(clk_in)
    begin
        if (rising_edge(clk_in)) then
            if (instr = x"00000000") then
                reset<='1';
            end if;
        end if;
        if (falling_edge(clk_in)) then
            reset<='0';
        end if;
        
    end process PC_reset;
end Behavioral;
