library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity ALUControl is
  Port (clk      : in     STD_LOGIC;
        opcode   : in     STD_LOGIC_VECTOR(5 downto 0);
        funct    : in     STD_LOGIC_VECTOR(5 downto 0);
        alu_crtl : out    STD_LOGIC_VECTOR(4 downto 0));
end ALUControl;

architecture Behavioral of ALUControl is

	constant  and_code 	: STD_LOGIC_VECTOR(4 downto 0):="00000";
	constant  or_code  	: STD_LOGIC_VECTOR(4 downto 0):="00001";
	constant  add_code 	: STD_LOGIC_VECTOR(4 downto 0):="00010";
	constant  sub_code 	: STD_LOGIC_VECTOR(4 downto 0):="00011";
	constant  addu_code : STD_LOGIC_VECTOR(4 downto 0):="00100";
	constant  nor_code  : STD_LOGIC_VECTOR(4 downto 0):="00101";
	constant  subu_code : STD_LOGIC_VECTOR(4 downto 0):="00110";
	constant  xor_code 	: STD_LOGIC_VECTOR(4 downto 0):="00111";
	constant  sllv_code : STD_LOGIC_VECTOR(4 downto 0):="01000";
	constant  srav_code : STD_LOGIC_VECTOR(4 downto 0):="01001";
	constant  srlv_code : STD_LOGIC_VECTOR(4 downto 0):="01010";
	constant  beq_code 	: STD_LOGIC_VECTOR(4 downto 0):="01011";
	constant  bgez_code : STD_LOGIC_VECTOR(4 downto 0):="01100";
	constant  bgtz_code : STD_LOGIC_VECTOR(4 downto 0):="01101";
	constant  blez_code : STD_LOGIC_VECTOR(4 downto 0):="01110";
	constant  bltz_code : STD_LOGIC_VECTOR(4 downto 0):="01111";
	constant  bne_code 	: STD_LOGIC_VECTOR(4 downto 0):="10000";
	constant  lui_code  : STD_LOGIC_VECTOR(4 downto 0):="10001";
	constant  multu_code : STD_LOGIC_VECTOR(4 downto 0):="10010";
	constant  divu_code  : STD_LOGIC_VECTOR(4 downto 0):="10100";
	constant  mfhi_code : STD_LOGIC_VECTOR(4 downto 0):="10101";
	constant  mflo_code : STD_LOGIC_VECTOR(4 downto 0):="10110";
	constant  mthi_code : STD_LOGIC_VECTOR(4 downto 0):="10111";
	constant  mtlo_code : STD_LOGIC_VECTOR(4 downto 0):="11000";                                                      
	constant  div_code : STD_LOGIC_VECTOR(4 downto 0):="11001";
	constant  mult_code : STD_LOGIC_VECTOR(4 downto 0):="11010";
--	constant  _code : STD_LOGIC_VECTOR(4 downto 0):="11011";
--	constant  _code : STD_LOGIC_VECTOR(4 downto 0):="11100";
--	constant  _code : STD_LOGIC_VECTOR(4 downto 0):="11101";
--	constant  _code : STD_LOGIC_VECTOR(4 downto 0):="11110";
--	constant  _code : STD_LOGIC_VECTOR(4 downto 0):="11111";




begin

alu_instruction:process(clk)
begin
    if(rising_edge(clk)) then
      case (opcode) is
      	when "000000" => -- standard arithmatic
          case (funct) is
            when "100000" => --ADD
              	alu_crtl <= add_code;
            when "100010" => --SUB
              	alu_crtl <= sub_code;
            when "100100" => --AND
             	alu_crtl <= and_code;
            when "100101" => --OR
             	alu_crtl <= or_code;
            when "100001" => --ADDU
            	alu_crtl <= addu_code;
            when "100111" => --NOR
                alu_crtl <= nor_code;
            when "100011" => --SUBU
                alu_crtl <= subu_code;
            when "100110" => --XOR
                alu_crtl <= xor_code;
            when "000100" => --SLLV
                alu_crtl <= sllv_code;
            when "000111" => --SRAV
                alu_crtl <= srav_code;
            when "000110" => --SRLV
                alu_crtl <= srlv_code;
            when "011011" => --DIVU
                alu_crtl <= divu_code;
            when "011001" => --MULTU
                alu_crtl <= multu_code;   
            when "010000" => --MFHI
                alu_crtl <= mfhi_code;   
            when "010010" => --MFLO    
                alu_crtl <= mflo_code; 
            when "010001" => --MTHI
                alu_crtl <= mthi_code;     
            when "010011" => --MTLO
                alu_crtl <= mtlo_code;
            when "011000" => --MTLO
                alu_crtl <= mult_code;
            when "011010" => --MTLO  
                alu_crtl <= div_code;                     
            when others =>
              alu_crtl <= and_code;
          end case;
		when "001000" => -- ADDI
           	alu_crtl <= add_code;
        when "100011" => -- LW
            alu_crtl <= add_code;
        when "101011" => -- SW
            alu_crtl <= add_code;
        when "000100" => -- BEQ
            alu_crtl <= beq_code;
        when "000001" => -- BGEZ, BLTZ
        	alu_crtl <= bgez_code;
        when "000111" => -- BGTZ
            alu_crtl <= bgtz_code;
        when "000110" => -- BLEZ
            alu_crtl <= blez_code;
        when "000101" => -- BNE
            alu_crtl <= bne_code;
        when "001111" => -- LUI
            alu_crtl <= lui_code;
        when "001001" => -- ADDIU
            alu_crtl <= addu_code;
        when "001100" => -- ANDI
            alu_crtl <= and_code;
        when "001101" => -- ORI
            alu_crtl <= or_code;
        when "001110" => -- XORI
            alu_crtl <= xor_code;
        when others =>
          alu_crtl <= and_code;
      end case;
    end if;

end process alu_instruction;


end Behavioral;
