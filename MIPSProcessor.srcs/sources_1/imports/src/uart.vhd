library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity UART is
  generic(
    CLKS_PER_BIT : integer := 868
    -- the number of clock cycles per bit that is being received
    -- this value is calculated by dividing the clock cycle by the UART baudrate
    -- in this case 100MHz / 115200 baud = 868
    );

  port(
    clk: 		IN 		STD_LOGIC;
    RX:			IN		STD_LOGIC;
    RX_ready:	        OUT             STD_LOGIC;
    RX_data:	        OUT             STD_LOGIC_VECTOR(7 downto 0)
    );
end UART;

architecture Behavioral of UART is
begin

  UART_RX:process(clk)
    -- declare a type for our statemachine
    type UART_RX_STATE is (IDLE,STARTBIT,RCVDATA,STOPBIT);
    variable state:UART_RX_STATE:=IDLE;

    variable data:STD_LOGIC_VECTOR(7 downto 0);
    variable databit_index:unsigned(3 downto 0):=(others=>'0');
    variable clk_divider:unsigned(9 downto 0):=(others=>'0');
  begin
    if rising_edge(clk) then
      clk_divider:=clk_divider+1;
      RX_ready<='0';

      case state is
        when IDLE => -- wait for RX to drop
          if RX = '0' then
            clk_divider:=(others=>'0'); -- reset counter
            state:=STARTBIT;
          end if;


        when STARTBIT => -- preparing to read out the data
          if (clk_divider=(CLKS_PER_BIT-1)/2) then -- halfway of the bit, good place to
                                               -- read out the data
            if (RX = '0') then -- RX should still be down
              clk_divider:=(others=>'0'); -- reset counter
              state:=RCVDATA;
            else
              state:=IDLE;
            end if;
          end if;


        when RCVDATA => -- reading out the bits
          if (clk_divider=CLKS_PER_BIT-1) then
            clk_divider:=(others=>'0'); -- reset counter

            data(to_integer(databit_index)):=RX; -- save the bit on the RX
            databit_index:=databit_index+1;

            if (databit_index=8) then -- received 8 bits
              state:=STOPBIT;
              databit_index:=(others=>'0'); -- reset index
            end if;
          end if;


      when STOPBIT => -- one stop bit
          if (clk_divider=CLKS_PER_BIT-1) then
            clk_divider:=(others=>'0'); -- reset counter

            RX_data<=data; -- cast the value of data to the output data lines
            RX_ready<='1'; -- one clock pulse, to signal that all 8 bits are
                           -- available on RX_data
            state:=IDLE;
          end if;
      end case;
    end if;
  end process UART_RX;

end Behavioral;
