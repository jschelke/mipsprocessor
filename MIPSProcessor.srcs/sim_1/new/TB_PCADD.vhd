LIBRARY ieee;
USE ieee.std_logic_1164.ALL;

ENTITY PCAdd_tb IS
END PCAdd_tb;

-- Component Declaration for the Unit Under Test (UUT)
architecture behavior of PCAdd_tb is
    component ProgramCounterAdd
    Port (  clk 		: in 	STD_LOGIC;
               PC_IN 	: in 	STD_LOGIC_VECTOR (31 downto 0);
               PC_OUT 	: out 	STD_LOGIC_VECTOR (31 downto 0);
               IR_IN 	: in 	STD_LOGIC_VECTOR (31 downto 0));
	end component ProgramCounterAdd;
	
    --Inputs
    signal clk: std_logic := '0';
    signal PC_IN : std_logic_vector(31 downto 0) := (others => '0');
    signal IR_IN : std_logic_vector(31 downto 0);
    
    --Outputs
    signal PC_OUT : std_logic_vector(31 downto 0);
    
    constant clk_period : time := 10 ns;
begin
    -- Instantiate the Unit Under Test (UUT)
    uut: ProgramCounterAdd
    port map (
    	clk		=>		clk,
    	PC_IN	=>		PC_IN,
        PC_OUT	=>		PC_OUT,
        IR_IN	=>		IR_IN
        );
    -- Clock process definitions
    clk_process:process
    begin
        clk <= '1' ;
        wait for clk_period/2;
        clk <= '0' ;
        wait for clk_period/2;
    end process;
    -- Stimulus process
    stim_proc:process
    begin
        -- hold reset state for 100 ns.
        wait for 100 ns;
        	PC_IN <= X"00000004";
        	IR_IN <= X"00000001";
        	wait for clk_period;
        	PC_IN <= X"00000008";
        	IR_IN <= X"00000002";
        wait for clk_period*10;

        wait;
    end process;
end;