LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
ENTITY instr_tb IS
END instr_tb;
-- Component Declaration for the Unit Under Test (UUT)
architecture behavior of instr_tb is
    component control_Top is
      Port (clk		 : in 	STD_LOGIC);
    end component control_Top;

    --Inputs
    signal clk: std_logic := '0';

    constant clk_period : time := 10 ns;
begin
    -- Instantiate the Unit Under Test (UUT)
    uut : control_Top
      port map (
        clk      =>    clk);

    -- Clock process definitions
    clk_process:process
    begin
        clk <= '1' ;
        wait for clk_period/2;
        clk <= '0' ;
        wait for clk_period/2;
    end process;
    -- Stimulus process
    stim_proc:process
    begin
        -- hold reset state for 100 ns.
        wait for 100 ns;
        wait for clk_period;
        wait for clk_period;
        wait for clk_period;

        wait;
    end process;
end;
