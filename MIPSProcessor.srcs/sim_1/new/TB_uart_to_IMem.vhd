library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

ENTITY uart_to_IMem_tb IS
END uart_to_Imem_tb;
 
ARCHITECTURE behavior OF uart_to_Imem_tb IS
 
	-- Component Declaration for the Unit Under Test (UUT)
 
COMPONENT uart_to_Imem
  PORT (
      clk:           in              STD_LOGIC;
      RX:            in              STD_LOGIC;
      instr_ready:   OUT             STD_LOGIC;
      instr:         OUT             STD_LOGIC_VECTOR(31 downto 0)
      );
END component;
 
--Inputs
signal clk : std_logic := '0';
signal RX  : std_logic := '1';
--Outputs
signal instr_ready:       STD_LOGIC;
signal instr:             STD_LOGIC_VECTOR(31 downto 0);

-- Clock period definitions
constant clk_period : time := 10 ns;

BEGIN
  -- Instantiate the Unit Under Test (UUT)
  uut: uart_to_Imem PORT MAP (
    clk => clk,
    RX => RX,
    instr_ready => instr_ready,
    instr => instr
    );
  -- Clock process definitions
	clk_process :process
	begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
	end process;
 
 
	-- Stimulus process
	stim_proc: process
	begin
		-- hold reset state for at least 100 ns.
		RX<='1';
		wait for 1 ms;
		
                -- LSB first!

                
----    "00100000 00000001 00000000 00000100",    -- ADDI $t0,$t1,4
----    "00100000 00000010 00000000 00000110",    -- ADDI $t0,$t2,6

                
        -- ######## "00100000 00000001 00000000 00000100" ########
        -- head 0xAA
        RX<='0'; -- STARTBIT
        wait for clk_period*868;
        RX<='0';
        wait for clk_period*868;
        RX<='1';
        wait for clk_period*868;
        RX<='0';
        wait for clk_period*868;
        RX<='1';
        wait for clk_period*868;
        RX<='0';
        wait for clk_period*868;
        RX<='1';
        wait for clk_period*868;
        RX<='0';
        wait for clk_period*868;
        RX<='1';
        wait for clk_period*868;
        RX<='1'; -- STOPBIT
        wait for clk_period*868;
        
	-- first byte is 00100000
	RX<='0'; -- STARTBIT
        wait for clk_period*868;
        RX<='0';
        wait for clk_period*868;
        RX<='0';
        wait for clk_period*868;
        RX<='0';
        wait for clk_period*868;
        RX<='0';
        wait for clk_period*868;
        RX<='0';
        wait for clk_period*868;
        RX<='1';
        wait for clk_period*868;
        RX<='0';
        wait for clk_period*868;
        RX<='0';
        wait for clk_period*868;
        RX<='1'; -- STOPBIT
        wait for clk_period*868;
        
        -- second byte is 00000001
        RX<='0'; -- STARTBIT
        wait for clk_period*868;
        RX<='1';
        wait for clk_period*868;
        RX<='0';
        wait for clk_period*868;
        RX<='0';
        wait for clk_period*868;
        RX<='0';
        wait for clk_period*868;
        RX<='0';
        wait for clk_period*868;
        RX<='0';
        wait for clk_period*868;
        RX<='0';
        wait for clk_period*868;
        RX<='0';
        wait for clk_period*868;
	RX<='1'; -- STOPBIT
        wait for clk_period*868;
        
        -- third byte is 00000000
        RX<='0'; -- STARTBIT
        wait for clk_period*868;
        RX<='0';
        wait for clk_period*868;
        RX<='0';
        wait for clk_period*868;
        RX<='0';
        wait for clk_period*868;
        RX<='0';
        wait for clk_period*868;
        RX<='0';
        wait for clk_period*868;
        RX<='0';
        wait for clk_period*868;
        RX<='0';
        wait for clk_period*868;
        RX<='0';
        wait for clk_period*868;
        RX<='1'; -- STOPBIT
        wait for clk_period*868;
        
        -- fourth byte is 00000100
        RX<='0'; -- STARTBIT
        wait for clk_period*868;
        RX<='0';
        wait for clk_period*868;
        RX<='0';
        wait for clk_period*868;
        RX<='1';
        wait for clk_period*868;
        RX<='0';
        wait for clk_period*868;
        RX<='0';
        wait for clk_period*868;
        RX<='0';
        wait for clk_period*868;
        RX<='0';
        wait for clk_period*868;
        RX<='0';
        wait for clk_period*868;
        RX<='1'; -- STOPBIT
        wait for clk_period*868;
        
        
        -- ######## "00100000 00000010 00000000 00000110" ########
        -- head 0xAA
        RX<='0'; -- STARTBIT
        wait for clk_period*868;
        RX<='0';
        wait for clk_period*868;
        RX<='1';
        wait for clk_period*868;
        RX<='0';
        wait for clk_period*868;
        RX<='1';
        wait for clk_period*868;
        RX<='0';
        wait for clk_period*868;
        RX<='1';
        wait for clk_period*868;
        RX<='0';
        wait for clk_period*868;
        RX<='1';
        wait for clk_period*868;
        RX<='1'; -- STOPBIT
        wait for clk_period*868;
        
	-- first byte is 00100000
	RX<='0'; -- STARTBIT
        wait for clk_period*868;
        RX<='0';
        wait for clk_period*868;
        RX<='0';
        wait for clk_period*868;
        RX<='0';
        wait for clk_period*868;
        RX<='0';
        wait for clk_period*868;
        RX<='0';
        wait for clk_period*868;
        RX<='1';
        wait for clk_period*868;
        RX<='0';
        wait for clk_period*868;
        RX<='0';
        wait for clk_period*868;
        RX<='1'; -- STOPBIT
        wait for clk_period*868;
        
        -- second byte is 00000010
        RX<='0'; -- STARTBIT
        wait for clk_period*868;
        RX<='0';
        wait for clk_period*868;
        RX<='1';
        wait for clk_period*868;
        RX<='0';
        wait for clk_period*868;
        RX<='0';
        wait for clk_period*868;
        RX<='0';
        wait for clk_period*868;
        RX<='0';
        wait for clk_period*868;
        RX<='0';
        wait for clk_period*868;
        RX<='0';
        wait for clk_period*868;
	RX<='1'; -- STOPBIT
        wait for clk_period*868;
        
        -- third byte is 00000000
        RX<='0'; -- STARTBIT
        wait for clk_period*868;
        RX<='0';
        wait for clk_period*868;
        RX<='0';
        wait for clk_period*868;
        RX<='0';
        wait for clk_period*868;
        RX<='0';
        wait for clk_period*868;
        RX<='0';
        wait for clk_period*868;
        RX<='0';
        wait for clk_period*868;
        RX<='0';
        wait for clk_period*868;
        RX<='0';
        wait for clk_period*868;
        RX<='1'; -- STOPBIT
        wait for clk_period*868;
        
        -- fourth byte is 00000110
        RX<='0'; -- STARTBIT
        wait for clk_period*868;
        RX<='0';
        wait for clk_period*868;
        RX<='1';
        wait for clk_period*868;
        RX<='1';
        wait for clk_period*868;
        RX<='0';
        wait for clk_period*868;
        RX<='0';
        wait for clk_period*868;
        RX<='0';
        wait for clk_period*868;
        RX<='0';
        wait for clk_period*868;
        RX<='0';
        wait for clk_period*868;
        RX<='1'; -- STOPBIT
        wait for clk_period*868;


        -- ######## "11101110 00000000 11101110 00000000" ########
        -- head 0xAA
        RX<='0'; -- STARTBIT
        wait for clk_period*868;
        RX<='0';
        wait for clk_period*868;
        RX<='1';
        wait for clk_period*868;
        RX<='0';
        wait for clk_period*868;
        RX<='1';
        wait for clk_period*868;
        RX<='0';
        wait for clk_period*868;
        RX<='1';
        wait for clk_period*868;
        RX<='0';
        wait for clk_period*868;
        RX<='1';
        wait for clk_period*868;
        RX<='1'; -- STOPBIT
        wait for clk_period*868;
        
	-- first byte is 11101110
	RX<='0'; -- STARTBIT
        wait for clk_period*868;
        RX<='0';
        wait for clk_period*868;
        RX<='1';
        wait for clk_period*868;
        RX<='1';
        wait for clk_period*868;
        RX<='1';
        wait for clk_period*868;
        RX<='0';
        wait for clk_period*868;
        RX<='1';
        wait for clk_period*868;
        RX<='1';
        wait for clk_period*868;
        RX<='1';
        wait for clk_period*868;
        RX<='1'; -- STOPBIT
        wait for clk_period*868;
        
        -- second byte is 00000000
        RX<='0'; -- STARTBIT
        wait for clk_period*868;
        RX<='0';
        wait for clk_period*868;
        RX<='0';
        wait for clk_period*868;
        RX<='0';
        wait for clk_period*868;
        RX<='0';
        wait for clk_period*868;
        RX<='0';
        wait for clk_period*868;
        RX<='0';
        wait for clk_period*868;
        RX<='0';
        wait for clk_period*868;
        RX<='0';
        wait for clk_period*868;
	RX<='1'; -- STOPBIT
        wait for clk_period*868;
        
        -- third byte is 11101110
	RX<='0'; -- STARTBIT
        wait for clk_period*868;
        RX<='0';
        wait for clk_period*868;
        RX<='1';
        wait for clk_period*868;
        RX<='1';
        wait for clk_period*868;
        RX<='1';
        wait for clk_period*868;
        RX<='0';
        wait for clk_period*868;
        RX<='1';
        wait for clk_period*868;
        RX<='1';
        wait for clk_period*868;
        RX<='1';
        wait for clk_period*868;
        RX<='1'; -- STOPBIT
        wait for clk_period*868;
        
        -- fourth byte is 00000000
        RX<='0'; -- STARTBIT
        wait for clk_period*868;
        RX<='0';
        wait for clk_period*868;
        RX<='0';
        wait for clk_period*868;
        RX<='0';
        wait for clk_period*868;
        RX<='0';
        wait for clk_period*868;
        RX<='0';
        wait for clk_period*868;
        RX<='0';
        wait for clk_period*868;
        RX<='0';
        wait for clk_period*868;
        RX<='0';
        wait for clk_period*868;
	RX<='1'; -- STOPBIT
        wait for clk_period*868;

        wait for clk_period*868;
		wait;
	end process;
END;
