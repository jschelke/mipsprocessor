LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
ENTITY Reg_tb IS
END Reg_tb;
-- Component Declaration for the Unit Under Test (UUT)
architecture behavior of Reg_tb is
    component Registers is
    port (  clk          : in    STD_LOGIC;
            write_enable : in    STD_LOGIC;
            Rs           : in    STD_LOGIC_VECTOR(4 downto 0);
            Rt           : in    STD_LOGIC_VECTOR(4 downto 0);
            Rd           : in    STD_LOGIC_VECTOR(4 downto 0);
            data_in      : in    STD_LOGIC_VECTOR(31 downto 0);
            out1         : out   STD_LOGIC_VECTOR(31 downto 0);
            out2         : out   STD_LOGIC_VECTOR(31 downto 0)
            );
    end component Registers;

    --Inputs
    signal clk: std_logic := '0';
    signal write_enable: std_logic := '0';
    signal Rs : std_logic_vector(4 downto 0) := (others => '0' );
    signal Rt : std_logic_vector(4 downto 0) := (others => '0' );
    signal Rd : std_logic_vector(4 downto 0) := (others => '0' );
    signal data_in : std_logic_vector(31 downto 0) := (others => '0' );
    --Outputs
    signal out1 : std_logic_vector(31 downto 0);
    signal out2 : std_logic_vector(31 downto 0);

    constant clk_period : time := 10 ns;

begin
    -- Instantiate the Unit Under Test (UUT)
    uut: Registers
    port map (
      --port                    signal
    	clk		=>      clk,
        write_enable    =>      write_enable,
        Rs              =>      Rs,
        Rt              =>      Rt,
        Rd              =>      Rd,
        data_in         =>      data_in,
        out1            =>      out1,
        out2            =>      out2);
    -- Clock process definitions
    clk_process:process
    begin
        clk <= '1' ;
        wait for clk_period/2;
        clk <= '0' ;
        wait for clk_period/2;
    end process;
    -- Stimulus process
    stim_proc:process
    begin
        -- hold reset state for 100 ns.
        wait for 100 ns;
        write_enable <= '1';
        rd <= "00010";
        data_in <= X"00001234";
        wait for clk_period;
        rd <= "00110";
        data_in <= X"011012B4";
        wait for clk_period;
        rd <= "10110";
        data_in <= X"0110400A";
        wait for clk_period*10;
        write_enable <= '0';
        rd <= "00000";
        rs <= "00110";
        rt <= "00010";
        wait;
    end process;
end;
