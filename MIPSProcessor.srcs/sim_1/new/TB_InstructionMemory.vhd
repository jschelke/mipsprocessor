LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
ENTITY IMem_tb IS
END IMem_tb;
-- Component Declaration for the Unit Under Test (UUT)
architecture behavior of IMem_tb is
    component InstructionMemory
    Port (  clk         : in    STD_LOGIC;
            Adress      : in    STD_LOGIC_VECTOR(31 downto 0);
            OPcode      : out   STD_LOGIC_VECTOR(31 downto 0));
	end component InstructionMemory;
    --Inputs
    signal clk: std_logic := '0';
    signal PC : std_logic_vector(31 downto 0) := (others => '0' );
    --Outputs
    signal Instruction : std_logic_vector(31 downto 0);
    constant clk_period : time := 10 ns;
begin
    -- Instantiate the Unit Under Test (UUT)
    uut: InstructionMemory
    port map (
    	clk		=>		clk,
    	Adress	=>		PC,
        OPcode	=>		Instruction
        );
    -- Clock process definitions
    clk_process:process
    begin
        clk <= '1' ;
        wait for clk_period/2;
        clk <= '0' ;
        wait for clk_period/2;
    end process;
    -- Stimulus process
    stim_proc:process
    begin
        -- hold reset state for 100 ns.
        wait for 100 ns;
        	PC <= X"00000004";
        	wait for clk_period;
        	PC <= X"00000008";
        	        	wait for clk_period;
            PC <= X"00000012";
                    	wait for clk_period;
            PC <= X"00000016";
        wait for clk_period*10;

        wait;
    end process;
end;