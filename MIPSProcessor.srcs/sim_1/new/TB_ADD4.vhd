LIBRARY ieee;
USE ieee.std_logic_1164.ALL;

ENTITY tb_add4 IS
END tb_add4;

-- Component Declaration for the Unit Under Test (UUT)
architecture behavior of tb_add4 is
    component ProgramCounterINC4
    Port (  clk         : in    STD_LOGIC;
        	PC_IN       : in    STD_LOGIC_VECTOR(31 downto 0);
    		PC_OUT      : out   STD_LOGIC_VECTOR(31 downto 0));
	end component ProgramCounterINC4;
	
    --Inputs
    signal clk: std_logic := '0';
    signal PC_IN : std_logic_vector(31 downto 0) := (others => '0' );
    
    --Outputs
    signal PC_OUT : std_logic_vector(31 downto 0);
    
    constant clk_period : time := 10 ns;
begin
        -- Instantiate the Unit Under Test (UUT)
        uut: ProgramCounterINC4
        port map (
        	clk		=>		clk,
        	PC_IN	=>		PC_IN,
            PC_OUT	=>		PC_OUT
            );
        -- Clock process definitions
        clk_process:process
        begin
            clk <= '1' ;
            wait for clk_period/2;
            clk <= '0' ;
            wait for clk_period/2;
        end process;
        -- Stimulus process
        stim_proc:process
        begin
            -- hold reset state for 100 ns.
            wait for 100 ns;
            	PC_IN <= X"00000004";
            	wait for clk_period;
            	PC_IN <= X"00000008";
            wait for clk_period*10;
    
            wait;
        end process;
 end;