LIBRARY ieee;
USE ieee.std_logic_1164.ALL;

ENTITY Control_tb IS
END Control_tb;

-- Component Declaration for the Unit Under Test (UUT)
architecture behavior of Control_tb is
	
	component Control_Top is
	    Port ( 	clk 			: in 	STD_LOGIC;
	    		PC_OUT2 		: out 	STD_LOGIC_VECTOR(31 downto 0);
	    		IR_OUT2			: out 	STD_LOGIC_VECTOR(31 downto 0);
	    		ALU_IN1		    : out 	STD_LOGIC_VECTOR(31 downto 0):= (others => '0');
                ALU_IN2         : out   STD_LOGIC_VECTOR(31 downto 0):= (others => '0');
                ProcData_OUT    : out   STD_LOGIC_VECTOR(31 downto 0):= (others => '0');
                RX              : in    STD_LOGIC
	    );
	end component Control_Top;
   
    --Inputs
    signal clk			: std_logic := '0';
    signal clk_stop		: std_logic := '0';
    signal clk_out		: std_logic := '0';
    
    signal RX           : std_logic := '1';
    
    --Outputs
    signal PC_OUT 		: std_logic_vector(31 downto 0) := (others => '0');
    signal IR_OUT 		: std_logic_vector(31 downto 0) := (others => '0');
    signal ALU_IN1      : std_logic_vector(31 downto 0) := (others => '0');
    signal ALU_IN2      : std_logic_vector(31 downto 0) := (others => '0');
    signal ProcData_OUT : std_logic_vector(31 downto 0) := (others => '0');

    
    constant clk_period : time := 10 ns;
begin
    -- Instantiate the Unit Under Test (UUT)
    uut: Control_Top
    port map (
    	clk			  =>	clk_out,
    	PC_OUT2		  =>	PC_OUT,
    	IR_OUT2		  =>	IR_OUT,
        ALU_IN1       =>    ALU_IN1,
        ALU_IN2       =>    ALU_IN2,
        ProcData_OUT  =>    ProcData_OUT,
        RX            =>    RX
        );
    -- Clock process definitions
    clk_process:process
    begin
        clk_out <= ('1' and clk_stop) ;
        wait for clk_period/2;
        clk_out <= ('0' and clk_stop);
        wait for clk_period/2;
    end process;
    -- Stimulus process
    stim_proc:process
    begin
        -- hold reset state for 100 ns.
        wait for 100 ns;
        clk_stop <= '1';
        RX<='1';
        wait for 500ns;
        
        
        -- send new instructions over UART
        
        ----    "00100000 00000001 00000000 00000100",    -- ADDI $t0,$t1,4
        ----    "00100000 00000010 00000000 00000110",    -- ADDI $t0,$t2,6
        
                        
                -- ######## "00100000 00000001 00000000 00000100" ########
                -- head 0xAA
                RX<='0'; -- STARTBIT
                wait for clk_period*868;
                RX<='0';
                wait for clk_period*868;
                RX<='1';
                wait for clk_period*868;
                RX<='0';
                wait for clk_period*868;
                RX<='1';
                wait for clk_period*868;
                RX<='0';
                wait for clk_period*868;
                RX<='1';
                wait for clk_period*868;
                RX<='0';
                wait for clk_period*868;
                RX<='1';
                wait for clk_period*868;
                RX<='1'; -- STOPBIT
                wait for clk_period*868;
                
            -- first byte is 00100000
            RX<='0'; -- STARTBIT
                wait for clk_period*868;
                RX<='0';
                wait for clk_period*868;
                RX<='0';
                wait for clk_period*868;
                RX<='0';
                wait for clk_period*868;
                RX<='0';
                wait for clk_period*868;
                RX<='0';
                wait for clk_period*868;
                RX<='1';
                wait for clk_period*868;
                RX<='0';
                wait for clk_period*868;
                RX<='0';
                wait for clk_period*868;
                RX<='1'; -- STOPBIT
                wait for clk_period*868;
                
                -- second byte is 00000001
                RX<='0'; -- STARTBIT
                wait for clk_period*868;
                RX<='1';
                wait for clk_period*868;
                RX<='0';
                wait for clk_period*868;
                RX<='0';
                wait for clk_period*868;
                RX<='0';
                wait for clk_period*868;
                RX<='0';
                wait for clk_period*868;
                RX<='0';
                wait for clk_period*868;
                RX<='0';
                wait for clk_period*868;
                RX<='0';
                wait for clk_period*868;
            RX<='1'; -- STOPBIT
                wait for clk_period*868;
                
                -- third byte is 00000000
                RX<='0'; -- STARTBIT
                wait for clk_period*868;
                RX<='0';
                wait for clk_period*868;
                RX<='0';
                wait for clk_period*868;
                RX<='0';
                wait for clk_period*868;
                RX<='0';
                wait for clk_period*868;
                RX<='0';
                wait for clk_period*868;
                RX<='0';
                wait for clk_period*868;
                RX<='0';
                wait for clk_period*868;
                RX<='0';
                wait for clk_period*868;
                RX<='1'; -- STOPBIT
                wait for clk_period*868;
                
                -- fourth byte is 00000100
                RX<='0'; -- STARTBIT
                wait for clk_period*868;
                RX<='0';
                wait for clk_period*868;
                RX<='0';
                wait for clk_period*868;
                RX<='1';
                wait for clk_period*868;
                RX<='0';
                wait for clk_period*868;
                RX<='0';
                wait for clk_period*868;
                RX<='0';
                wait for clk_period*868;
                RX<='0';
                wait for clk_period*868;
                RX<='0';
                wait for clk_period*868;
                RX<='1'; -- STOPBIT
                wait for clk_period*868;
                
                
                -- ######## "00100000 00000010 00000000 00000110" ########
                -- head 0xAA
                RX<='0'; -- STARTBIT
                wait for clk_period*868;
                RX<='0';
                wait for clk_period*868;
                RX<='1';
                wait for clk_period*868;
                RX<='0';
                wait for clk_period*868;
                RX<='1';
                wait for clk_period*868;
                RX<='0';
                wait for clk_period*868;
                RX<='1';
                wait for clk_period*868;
                RX<='0';
                wait for clk_period*868;
                RX<='1';
                wait for clk_period*868;
                RX<='1'; -- STOPBIT
                wait for clk_period*868;
                
            -- first byte is 00100000
            RX<='0'; -- STARTBIT
                wait for clk_period*868;
                RX<='0';
                wait for clk_period*868;
                RX<='0';
                wait for clk_period*868;
                RX<='0';
                wait for clk_period*868;
                RX<='0';
                wait for clk_period*868;
                RX<='0';
                wait for clk_period*868;
                RX<='1';
                wait for clk_period*868;
                RX<='0';
                wait for clk_period*868;
                RX<='0';
                wait for clk_period*868;
                RX<='1'; -- STOPBIT
                wait for clk_period*868;
                
                -- second byte is 00000010
                RX<='0'; -- STARTBIT
                wait for clk_period*868;
                RX<='0';
                wait for clk_period*868;
                RX<='1';
                wait for clk_period*868;
                RX<='0';
                wait for clk_period*868;
                RX<='0';
                wait for clk_period*868;
                RX<='0';
                wait for clk_period*868;
                RX<='0';
                wait for clk_period*868;
                RX<='0';
                wait for clk_period*868;
                RX<='0';
                wait for clk_period*868;
            RX<='1'; -- STOPBIT
                wait for clk_period*868;
                
                -- third byte is 00000000
                RX<='0'; -- STARTBIT
                wait for clk_period*868;
                RX<='0';
                wait for clk_period*868;
                RX<='0';
                wait for clk_period*868;
                RX<='0';
                wait for clk_period*868;
                RX<='0';
                wait for clk_period*868;
                RX<='0';
                wait for clk_period*868;
                RX<='0';
                wait for clk_period*868;
                RX<='0';
                wait for clk_period*868;
                RX<='0';
                wait for clk_period*868;
                RX<='1'; -- STOPBIT
                wait for clk_period*868;
                
                -- fourth byte is 00000110
                RX<='0'; -- STARTBIT
                wait for clk_period*868;
                RX<='0';
                wait for clk_period*868;
                RX<='1';
                wait for clk_period*868;
                RX<='1';
                wait for clk_period*868;
                RX<='0';
                wait for clk_period*868;
                RX<='0';
                wait for clk_period*868;
                RX<='0';
                wait for clk_period*868;
                RX<='0';
                wait for clk_period*868;
                RX<='0';
                wait for clk_period*868;
                RX<='1'; -- STOPBIT
                wait for clk_period*868;
        
        
                -- ######## "11101110 00000000 11101110 00000000" ########
                -- head 0xAA
                RX<='0'; -- STARTBIT
                wait for clk_period*868;
                RX<='0';
                wait for clk_period*868;
                RX<='1';
                wait for clk_period*868;
                RX<='0';
                wait for clk_period*868;
                RX<='1';
                wait for clk_period*868;
                RX<='0';
                wait for clk_period*868;
                RX<='1';
                wait for clk_period*868;
                RX<='0';
                wait for clk_period*868;
                RX<='1';
                wait for clk_period*868;
                RX<='1'; -- STOPBIT
                wait for clk_period*868;
                
            -- first byte is 11101110
            RX<='0'; -- STARTBIT
                wait for clk_period*868;
                RX<='0';
                wait for clk_period*868;
                RX<='1';
                wait for clk_period*868;
                RX<='1';
                wait for clk_period*868;
                RX<='1';
                wait for clk_period*868;
                RX<='0';
                wait for clk_period*868;
                RX<='1';
                wait for clk_period*868;
                RX<='1';
                wait for clk_period*868;
                RX<='1';
                wait for clk_period*868;
                RX<='1'; -- STOPBIT
                wait for clk_period*868;
                
                -- second byte is 00000000
                RX<='0'; -- STARTBIT
                wait for clk_period*868;
                RX<='0';
                wait for clk_period*868;
                RX<='0';
                wait for clk_period*868;
                RX<='0';
                wait for clk_period*868;
                RX<='0';
                wait for clk_period*868;
                RX<='0';
                wait for clk_period*868;
                RX<='0';
                wait for clk_period*868;
                RX<='0';
                wait for clk_period*868;
                RX<='0';
                wait for clk_period*868;
            RX<='1'; -- STOPBIT
                wait for clk_period*868;
                
                -- third byte is 11101110
            RX<='0'; -- STARTBIT
                wait for clk_period*868;
                RX<='0';
                wait for clk_period*868;
                RX<='1';
                wait for clk_period*868;
                RX<='1';
                wait for clk_period*868;
                RX<='1';
                wait for clk_period*868;
                RX<='0';
                wait for clk_period*868;
                RX<='1';
                wait for clk_period*868;
                RX<='1';
                wait for clk_period*868;
                RX<='1';
                wait for clk_period*868;
                RX<='1'; -- STOPBIT
                wait for clk_period*868;
                
                -- fourth byte is 00000000
                RX<='0'; -- STARTBIT
                wait for clk_period*868;
                RX<='0';
                wait for clk_period*868;
                RX<='0';
                wait for clk_period*868;
                RX<='0';
                wait for clk_period*868;
                RX<='0';
                wait for clk_period*868;
                RX<='0';
                wait for clk_period*868;
                RX<='0';
                wait for clk_period*868;
                RX<='0';
                wait for clk_period*868;
                RX<='0';
                wait for clk_period*868;
            RX<='1'; -- STOPBIT
                wait for clk_period*868;
        
                wait for clk_period*868;
                
                wait;
    end process;
end;