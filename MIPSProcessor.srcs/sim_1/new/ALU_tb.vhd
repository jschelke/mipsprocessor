LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
ENTITY alu_tb IS
END alu_tb;
-- Component Declaration for the Unit Under Test (UUT)
architecture behavior of alu_tb is
    component ALUControl is
      Port (clk      : in     STD_LOGIC;
            alu_op   : in     STD_LOGIC_VECTOR(1 downto 0);
            funct    : in     STD_LOGIC_VECTOR(5 downto 0);
            alu_crtl : out    STD_LOGIC_VECTOR(3 downto 0));
    end component ALUControl;
	component ALU is
      port (clk          : in    STD_LOGIC;
            alu_crtl     : in    STD_LOGIC_VECTOR(3 downto 0);
            in1          : in    STD_LOGIC_VECTOR(31 downto 0);
            in2          : in    STD_LOGIC_VECTOR(31 downto 0);
            result       : out   STD_LOGIC_VECTOR(31 downto 0);
            zero         : out   STD_LOGIC);
    end component ALU;
    --Inputs
    signal clk: std_logic := '0';
    signal in_alu_op : std_logic_vector(1 downto 0) := (others => '0' );
    signal in_funct  : std_logic_vector(5 downto 0) := (others => '0' );
    signal in_data1  : std_logic_vector(31 downto 0) := (others => '0' );
    signal in_data2  : std_logic_vector(31 downto 0) := (others => '0' );
    --inbetweenie
    signal tb_alu_crtl : std_logic_vector(3 downto 0) := (others => '0' );
    --Outputs
    signal out_result   : std_logic_vector(31 downto 0) := (others => '0' );
    signal out_zero     : std_logic := '0';
    constant clk_period : time := 10 ns;
begin
    -- Instantiate the Unit Under Test (UUT)

    -- uut: ALUControl
    alucontrol_tb : alucontrol
    port map (
        clk      =>    clk,
        alu_op   =>    in_alu_op,
        funct    =>    in_funct,
        alu_crtl =>    tb_alu_crtl);

    -- uut: ALU
    alu_tb : alu
    port map (
        clk      =>    clk,
        alu_crtl =>    tb_alu_crtl,
        in1      =>    in_data1,
        in2      =>    in_data2,
        result   =>    out_result,
        zero     =>    out_zero);
    -- Clock process definitions
    clk_process:process
    begin
        clk <= '1' ;
        wait for clk_period/2;
        clk <= '0' ;
        wait for clk_period/2;
    end process;
    -- Stimulus process
    stim_proc:process
    begin
        -- hold reset state for 100 ns.
        wait for 100 ns;
        	in_data1 <= X"00000005";
        	in_data2 <= X"00000003";
        	in_alu_op <= "00";
        	in_funct <= "100000";
        	wait for clk_period;
        	in_alu_op <= "10";
        	in_funct <= "100010";
        	wait for clk_period;
            in_alu_op <= "10";
            in_funct <= "100100";
            wait for clk_period;
            in_alu_op <= "10";
            in_funct <= "100101";
        wait for clk_period*10;

        wait;
    end process;
end;
