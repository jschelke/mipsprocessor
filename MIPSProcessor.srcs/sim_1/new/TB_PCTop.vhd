LIBRARY ieee;
USE ieee.std_logic_1164.ALL;

ENTITY PCTop_tb IS
END PCTop_tb;

-- Component Declaration for the Unit Under Test (UUT)
architecture behavior of PCTop_tb is
	
	component PC_Top
	Port ( 	clk_in 			: in 	STD_LOGIC;
            PC_BR			: in 	STD_LOGIC_VECTOR(31 downto 0);
			PC_JMP			: in 	STD_LOGIC_VECTOR(31 downto 0);
			PCSrc			: in 	STD_LOGIC_VECTOR(1 downto 0);
			OpCode			: out 	STD_LOGIC_VECTOR(31 downto 0);
			IR_Done			: in 	STD_LOGIC; --Instruction is done now send new information
			IR_NewServed 	: out	STD_LOGIC;
			PC_Output		: out	STD_LOGIC_VECTOR(31 downto 0);
			RX              : in    STD_LOGIC);
   end component PC_Top;
   
    --Inputs
    signal clk			: std_logic := '0';
    signal PC_BR        : std_logic_vector(31 downto 0) := (others => '0');
    signal PC_JMP 		: std_logic_vector(31 downto 0) := (others => '0');
    signal PCSrc		: std_logic_vector(1 downto 0)  := (others => '0');
    signal IR_Done		: std_logic := '0';
    signal RX           : std_logic := '0';
    
    --Outputs
    signal OpCode 		: std_logic_vector(31 downto 0);
    signal IR_NewServed	: std_logic := '0';
    signal PC_Output	: STD_LOGIC_VECTOR(31 downto 0);
    
    constant clk_period : time := 10 ns;
begin
    -- Instantiate the Unit Under Test (UUT)
    uut: PC_Top
    port map (
    	clk_in			=>	clk,
    	PC_BR           =>  PC_BR,
    	PC_JMP			=>	PC_JMP,
        PCSrc			=>	PCSrc,
        OpCode			=>	OpCode,
        IR_Done			=>	IR_Done,
        IR_NewServed	=>	IR_NewServed,
        PC_Output		=>	PC_Output,
        RX              =>  RX
        );
    -- Clock process definitions
    clk_process:process
    begin
        clk <= '1' ;
        wait for clk_period/2;
        clk <= '0' ;
        wait for clk_period/2;
    end process;
    -- Stimulus process
    stim_proc:process
    begin
        -- hold reset state for 100 ns.
        wait for 100 ns;
        	PC_JMP <= x"00000002";
        	PCSrc <= b"01";
			IR_Done <= '1';
			wait for clk_period;
			IR_Done <= '0';
        wait for clk_period*5;
        	PC_JMP <= x"00000002";
            PCSrc <= b"01";
        	IR_Done <= '1';
        	wait for clk_period;
        	IR_Done <= '0';
        wait for clk_period*5;
            PCSrc <= b"00";
        	IR_Done <= '1';
        	wait for clk_period;
        	IR_Done <= '0';
        wait;
    end process;
end;